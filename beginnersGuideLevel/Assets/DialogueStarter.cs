﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueStarter : MonoBehaviour
{
    bool hasHit = false;

    void OnTriggerEnter(Collider other)
    {
        hasHit = true;
    }
    // Update is called once per frame
    public bool HasHit()
    {
        return hasHit;
    }
}
