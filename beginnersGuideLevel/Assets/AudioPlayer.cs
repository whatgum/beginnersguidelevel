﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioPlayer : MonoBehaviour
{
    bool played = false;
    // Start is called before the first frame update
    void OnTriggerEnter(Collider Other)
    {
        if(played == false)
        {
            played = true;
            gameObject.GetComponent<AudioSource>().Play();
        }
    }
    

}
