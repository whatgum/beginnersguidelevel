﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class telephoneBox : MonoBehaviour
{
    AudioSource[] audios;
    // Start is called before the first frame update
    void Start()
    {
        audios = gameObject.GetComponents<AudioSource>();
    }

    bool narrativedIsPlayed = false;
    // Update is called once per frame
    public void PlayNarrative()
    {
        audios[1].Play();
        narrativedIsPlayed = true;
    }
    void Update()
    {
        if(narrativedIsPlayed == true && audios[1].isPlaying == false && once == true)
        {
            audios[0].mute = false;
        }

    }
    bool once = true;
    void OnTriggerEnter(Collider other)
    {
        if(once == true)
        {
            once = false;
            audios[0].mute = true;
        }
    }
    public bool checkOnce()
    {
        return once;
    }
}
