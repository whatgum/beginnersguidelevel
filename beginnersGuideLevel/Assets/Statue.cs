﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Statue : MonoBehaviour
{
    bool startDecending = false;
    Dialogue dialogue;

    // Start is called before the first frame update
    void Start()
    {
        dialogue = GameObject.FindWithTag("Canvas").GetComponent<Dialogue>();
    }

    // Update is called once per frame
    void Update()
    {
        if(startDecending == true && gameObject.transform.position.y > 17)
        {
            gameObject.transform.position = gameObject.transform.position - new Vector3(0f,.022f, 0f);
        }
        startDecending = dialogue.FinishedFirstDialgoue();
    }
}
