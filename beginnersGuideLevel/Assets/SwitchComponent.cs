﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchComponent : MonoBehaviour
{
    int maxIndex = 3;
    int startingIndex = 0;
    Dialogue dialogue;
    void Start()
    {
        dialogue = GameObject.FindWithTag("Canvas").GetComponent<Dialogue>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            RaycastHit hitInfo = new RaycastHit();
            bool hit = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo);
            if(hit)
            {
                if(hitInfo.distance < 5)
                {
                    if(hitInfo.transform.gameObject.name == gameObject.name)
                    {
                        gameObject.transform.GetChild(startingIndex).gameObject.SetActive(false);
                        startingIndex++;
                        if(startingIndex == maxIndex)
                        {
                            startingIndex = 0;
                        }
                        gameObject.transform.GetChild(startingIndex).gameObject.SetActive(true);
                        dialogue.Insult();
                    }
                }
            }
        }
    }
}
