﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Dialogue : MonoBehaviour
{
    GameObject text1;
    GameObject text2;
    GameObject text3;
    GameObject response1;
    GameObject response2;
    GameObject response3;
    GameObject trigger;
    GameObject telephoneBox;
    bool start = false;
    Color ourTextColor = new Color(0.8113208f, 4901308f, 0.1454254f);
    Color theirTextColor = new Color(0.5849056f, 0.08642906f, 0.05517977f);
    void Start()
    {
        trigger = gameObject.transform.GetChild(1).gameObject;
        text1 = gameObject.transform.GetChild(0).transform.GetChild(0).gameObject;
        text2 = gameObject.transform.GetChild(0).transform.GetChild(1).gameObject;
        text3 = gameObject.transform.GetChild(0).transform.GetChild(2).gameObject;
        response1 = gameObject.transform.GetChild(2).transform.GetChild(0).gameObject;
        response2 = gameObject.transform.GetChild(2).transform.GetChild(1).gameObject;
        response3 = gameObject.transform.GetChild(2).transform.GetChild(2).gameObject;
        telephoneBox = GameObject.FindWithTag("telephone");

    }

    int insultIndex = 0;
    string insult1 = " Not good enough!";
    string insult2 = " Too ugly!";
    string insult3 = " Worthless!";
    string insult4 = " This is unacceptable!";
    string insult5 = " Try harder!";
    string insult6 = " Do better!";
    string insult7 = " Focus!";
    string insult8 = " Why can't you do better?";
    string insult9 = " This is terrible!";
    string insult10 = " Where is the originality?";
    string insult11 = " Every part is horrible!";
    int maxInsultIndex = 11;

    int[] responses = {0, 0, 0};
    string response1A = "1. I don't know.";
    string response1B = "2. Where is here?";
    string response1C = "3. I am looking for something.";
    string response2A = "1. Happiness.";
    string response2B = "2. Satisfaction.";
    string response2C = "3. Worth.";
    string response3A = "1. How?";
    string response3B = "2. I don't believe you.";
    string response3C = "3. Will it last?";
    string response4A = "1. It is impossible.";
    string response4B = "2. They must be tricking me.";
    string response4C = "3. Why can't I do it?";
    
    string question1 = " Why are you here?";
    string question2 = " What do you seek?";
    string question3 = " We can grant you this.";
    string question4 = " Still trying to please gods huh?";

    string theirResponse1A = " You may not know but we do.";
    string theirResponse1B = " You are in the realm of Gods.";
    string theirResponse3A = " Your belief doesn't dissuade the truth.";
    string theirResponse3B = " It will.";

    string statement1 = "All you have to do is please us.";
    string statement2 = "Turn this statue into beauty.";
    string statement3 = "The truth is, these gods aren't real.";

    bool waitingForButton = false;
    bool finishedDialogue1 = false;
    bool finished = false;
    // Update is called once per frame
    void Update()
    {
        if(telephoneBox.GetComponent<telephoneBox>().checkOnce() == false && swatch == false)
        {
            SwitchSwitch();
        }
        if(swatch == false)
        {
            if(start == true && responses[0] == 0 && waitingForButton == false)
            {
                waitingForButton = true;
                CreateChild(question1, true);
            }
            else if(waitingForButton == true && responses[0] == 0)
            {
                response1.GetComponent<Text>().text = response1A;
                response2.GetComponent<Text>().text = response1B;
                response3.GetComponent<Text>().text = response1C;
                if(Input.GetButtonDown("response1"))
                {
                    responses[0] = 1;
                    waitingForButton = false;
                }
                if(Input.GetButtonDown("response2"))
                {
                    responses[0] = 2;
                    waitingForButton = false;

                }
                if(Input.GetButtonDown("response3"))
                {
                    responses[0] = 3;
                    waitingForButton = false;
                }
            }
            else if(waitingForButton == false && responses[0] == 1 && responses[1] == 0)
            {
                string substring = response1A.Substring(2);
                CreateChild(substring, false);
                CreateChild(theirResponse1A, true);
                CreateChild(question2, true);
                waitingForButton = true;
            }
            else if(waitingForButton == false && responses[0] == 2 && responses[1] == 0)
            {
                string substring = response1B.Substring(2);
                CreateChild(substring, false);
                CreateChild(theirResponse1B, true);
                CreateChild(question2, true);
                waitingForButton = true;

            }
            else if(waitingForButton == false && responses[0] == 3 && responses[1] == 0)
            {
                string substring = response1C.Substring(2);
                CreateChild(substring, false);
                CreateChild(question2, true);
                waitingForButton = true;
            }

            else if(waitingForButton == true && responses[1] == 0)
            {
                response1.GetComponent<Text>().text = response2A;
                response2.GetComponent<Text>().text = response2B;
                response3.GetComponent<Text>().text = response2C;
                if(Input.GetButtonDown("response1"))
                {
                    responses[1] = 1;
                    waitingForButton = false;

                }
                if(Input.GetButtonDown("response2"))
                {
                    responses[1] = 2;
                    waitingForButton = false;

                }
                if(Input.GetButtonDown("response3"))
                {
                    responses[1] = 3;
                    waitingForButton = false;

                }
            }
            else if(waitingForButton == false && responses[1] == 1 && responses[2] == 0)
            {
                string substring = response2A.Substring(2);
                CreateChild(substring, false);
                CreateChild(question3, true);
                waitingForButton = true;
            }

            else if(waitingForButton == false && responses[1] == 2 && responses[2] == 0)
            {
                string substring = response2B.Substring(2);
                CreateChild(substring, false);
                CreateChild(question3, true);
                waitingForButton = true;
            }
            else if(waitingForButton == false && responses[1] == 3 && responses[2] == 0)
            {
                string substring = response2C.Substring(2);
                CreateChild(substring, false);
                CreateChild(question3, true);
                waitingForButton = true;
            }
            else if(waitingForButton == true && responses[2] == 0 && responses[2] == 0)
            {
                response1.GetComponent<Text>().text = response3A;
                response2.GetComponent<Text>().text = response3B;
                response3.GetComponent<Text>().text = response3C;
                if(Input.GetButtonDown("response1"))
                {
                    responses[2] = 1;
                    waitingForButton = false;
                    response1.GetComponent<Text>().text = "";
                    response2.GetComponent<Text>().text = "";
                    response3.GetComponent<Text>().text = "";
                }
                if(Input.GetButtonDown("response2"))
                {
                    responses[2] = 2;
                    waitingForButton = false;
                    response1.GetComponent<Text>().text = "";
                    response2.GetComponent<Text>().text = "";
                    response3.GetComponent<Text>().text = "";
                }
                if(Input.GetButtonDown("response3"))
                {
                    responses[2] = 3;
                    waitingForButton = false;
                    response1.GetComponent<Text>().text = "";
                    response2.GetComponent<Text>().text = "";
                    response3.GetComponent<Text>().text = "";
                }
            }
            else if(waitingForButton == false && responses[2] == 1)
            {
                string substring = response3A.Substring(2);
                CreateChild(substring, false);
                CreateChild(theirResponse3A, true);
                CreateChild(statement1, true);
                CreateChild(statement2, true);
                waitingForButton = true;
                finishedDialogue1 = true;

            }
            else if(waitingForButton == false && responses[2] == 2)
            {
                string substring = response3B.Substring(2);
                CreateChild(substring, false);
                CreateChild(theirResponse3B, true);
                CreateChild(statement1, true);
                CreateChild(statement2, true);
                waitingForButton = true;
                finishedDialogue1 = true;

            }
            else if(waitingForButton == false && responses[2] == 3)
            {
                string substring = response3C.Substring(2);
                CreateChild(substring, false);
                CreateChild(statement1 , true);
                CreateChild(statement2, true);
                waitingForButton = true;
                finishedDialogue1 = true;
            }
            else if(waitingForButton == false && responses[0] == 0)
            {
                start = trigger.GetComponent<DialogueStarter>().HasHit();
            }
        }
        else
        {
            
            if(waitingForButton == false && finished == false)
            {
                theirTextColor = new Color(0.1545034f, 0.5849056f, 0.3539538f);
                waitingForButton = true;
                CreateChild(question4, true);
            }
            else if(waitingForButton == true && finished == false)
            {
                response1.GetComponent<Text>().text = response4A;
                response2.GetComponent<Text>().text = response4B;
                response3.GetComponent<Text>().text = response4C;
                if(Input.GetButtonDown("response1"))
                {
                    responses[2] = 1;
                    waitingForButton = false;
                    response1.GetComponent<Text>().text = "";
                    response2.GetComponent<Text>().text = "";
                    response3.GetComponent<Text>().text = "";
                    finished = true;
                }
                if(Input.GetButtonDown("response2"))
                {
                    responses[2] = 2;
                    waitingForButton = false;
                    response1.GetComponent<Text>().text = "";
                    response2.GetComponent<Text>().text = "";
                    response3.GetComponent<Text>().text = "";
                    finished = true;
                }
                if(Input.GetButtonDown("response3"))
                {
                    responses[2] = 3;
                    waitingForButton = false;
                    response1.GetComponent<Text>().text = "";
                    response2.GetComponent<Text>().text = "";
                    response3.GetComponent<Text>().text = "";
                    finished = true;
                }
            }
            else if(waitingForButton == false && finished == true)
            {
                CreateChild(statement3, true);
                waitingForButton = true;
                GameObject.FindWithTag("magic").GetComponent<MagicTrick>().MagicTricks();
            }
        }
    }

    void CreateChild(string words, bool theirs)
    {
        Text thirdText = text1.GetComponent<Text>();
        Text secondText = text2.GetComponent<Text>();
        Text firstText = text3.GetComponent<Text>();
        Color colorForText;

        if(theirs)
        {
            colorForText = theirTextColor;
        }

        else
        {
            colorForText = ourTextColor;
        }
        thirdText.text = secondText.text;
        secondText.text = firstText.text;
        thirdText.color = secondText.color;
        secondText.color = firstText.color;
        firstText.color = colorForText;
        firstText.text = words;

    }
    public bool FinishedFirstDialgoue()
    {
        return finishedDialogue1;
    }   

    bool played = false;
    public void Insult()
    {
        if(insultIndex == 0)
        {
            CreateChild(insult1, true);
        }
        if(insultIndex == 1)
        {
            CreateChild(insult2, true);
        }
        if(insultIndex == 2)
        {
            CreateChild(insult3, true);
        }
         if(insultIndex == 3)
        {
            CreateChild(insult4, true);
        }
         if(insultIndex == 4)
        {
            CreateChild(insult5, true);
        }
         if(insultIndex == 5)
        {
            CreateChild(insult6, true);
            
        }
         if(insultIndex == 6)
        {
            CreateChild(insult7, true);
        }
         if(insultIndex == 7)
        {
            CreateChild(insult8, true);
        }
         if(insultIndex == 8)
        {
            CreateChild(insult9, true);
        }
         if(insultIndex == 9)
        {
            CreateChild(insult10, true);
        }
         if(insultIndex == 10)
        {
            CreateChild(insult11, true);
            if(played == false)
            {
                played = true;
                telephoneBox.GetComponent<telephoneBox>().PlayNarrative();
            }
        }
        insultIndex++;
        if(insultIndex == maxInsultIndex)
        {
            insultIndex = 0;
        }
    }
    bool swatch = false;
    void SwitchSwitch()
    {
        waitingForButton = false;
        swatch = true;
    }
}
