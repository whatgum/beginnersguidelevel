﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagicTrick : MonoBehaviour
{
    GameObject spotlights;
    Color PitchBlack = new Color(0f,0f,0f);
    void Start()
    {
        spotlights = GameObject.FindWithTag("spotLight");

    }
    public void MagicTricks()
    {
        spotlights.GetComponent<Light>().color = PitchBlack;
        gameObject.transform.GetChild(4).gameObject.SetActive(true);
        gameObject.transform.GetChild(3).gameObject.SetActive(true);

    }
}
